<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
============================================================
 This ExpressionEngine plugin was created by Leon Dijk
 - http://gwcode.com/
============================================================
 This plugin is licensed under a
 Creative Commons Attribution-NoDerivs 3.0 Unported License.
 - http://creativecommons.org/licenses/by-nd/3.0/
============================================================
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
============================================================
*/

$plugin_info = array(
	'pi_name'			=> 'BF - URI to ID',
	'pi_version'		=> '1',
	'pi_author'			=> 'Thomas Lesire',
	'pi_author_url'		=> 'http://ee.studiobreakfast.be/plugins/uri-to-id/',
	'pi_description'	=> 'Convert URI segment to entry_id.',
	'pi_usage'			=> bf_uri_to_id::usage()
);

class Bf_uri_to_id {

	private $page_uri = '';
	private $page_uri_clean = '';
	private $tagdata = '';
	private $variables = array();
	private $qry_entry_id = '';
	private $output = '';

	public function Bf_uri_to_id() {
		$this->__construct();
	}

	public function __construct() {

		$this->EE =& get_instance();

		// start: fetch & validate plugin params
		$this->page_uri = $this->EE->TMPL->fetch_param('uri');
		$this->page_uri_clean = $this->EE->db->escape_str($this->page_uri);

		if($this->page_uri_clean != '') {

			$this->tagdata = $this->EE->TMPL->tagdata;
			$this->variables = array();

			$this->qry_entry_id = $this->EE->db->get_where('channel_titles', array('url_title' => $this->page_uri_clean));

			if($this->qry_entry_id->num_rows() > 0) {

				foreach($this->qry_entry_id->result() as $row)
				{
					$this->variables[] = array(
						'entry_id' => $row->entry_id
					);
				}

				$this->EE->TMPL->log_item('URI2ID : '.$this->variables);

				$this->output = $this->EE->TMPL->parse_variables($this->tagdata, $this->variables);

			}
			
		} else {

			$this->EE->TMPL->log_item('URI2ID : the "uri" parameter value needs to be filled.');

		}

		$this->return_data = $this->output;

		

	} // end function __construct

	// ----------------------------------------
	// Plugin Usage
	// ----------------------------------------
	// This function describes how the plugin is used.
	function usage() {
		ob_start();
?>
The tag in its most simple form:
{exp:bf_uri_to_id}
..will show entry_id of page.

PARAMETERS:
uri

<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
} // END CLASS
?>