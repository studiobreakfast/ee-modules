<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Toggle Fields Accessory
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Thomas Lesire
 * @link		http://www.thomaslesire.be
 * @copyright 	Copyright (c) 2012 Thomas lesire
 * @license   	http://creativecommons.org/licenses/by-sa/3.0/ Attribution-Share Alike 3.0 Unported
 */

// ------------------------------------------------------------------------
 

class Bf_toggle_fields_acc {

	var $name			= 'BF / Toggle Fields';
	var $id				= 'bf_toggle_fields';
	var $version		= '1.0';
	var $description	= 'Creates links that show and hide all fields in the publish/edit form.';
	var $sections		= array();

	
	function __construct()
	{
		$this->EE =& get_instance();
	}
	

	function set_sections()
	{

		$this->EE->cp->add_to_head('
			<script>
				$(document).ready(function() {

					toggle = \'<a href="#" class="toggle-fieds">Expand / Collapse</a> | \';

					$("#showToolbarLink a").before(toggle);

					$(".toggle-fieds").click(function() {
						$("div[id*=sub_hold_field]").slideToggle();
					});
				});
			</script>
		');
	}
	
	function update()
	{
		return TRUE;
	}
	
}
/* End of file acc.bf_toggle_fields.php */
/* Location: /system/expressionengine/third_party/bf_toggle_fields/acc.bf_toggle_fields.php */